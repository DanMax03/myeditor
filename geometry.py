class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __neg__(self):
        return Point(-self.x, -self.y)

    def __add__(self, another):
        x = self.x + another.x
        y = self.y + another.y
        return Point(x, y)

    def __iadd__(self, another):
        self.x += another.x
        self.y += another.y
        return self

    def __sub__(self, another):
        return self + (-another)


def isInBorders(x, lt, rt):
    return lt <= x <= rt
