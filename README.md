# MyEditor
A Python project for the subject

## Installation
**requirements.txt** file contains a list of all required packages, which can be easily installed by `pip`.

To start the editor, use the command `<python3-interpeter> main.py`. For the command's options, check the `-h` flag


## Usage
I can't imagine that you won't have `vi` or `vim` in the console. Anyway,

- Press **ESC** to leave
- Use arrows to move the caret across a file
- Everything else works as usual: **ENTER** will make a new line, any other button will be considered as to print

## Roadmap
I will never write GUI or CLI on Python again...

## Authors and acknowledgment
DanMax03

## License
Coming soon

## Project status
No idea, really
