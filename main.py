import argparse
import pathlib as pl
import unicurses as uc
from filewindow import FileWindow
from random import randint


class MyEditor:
    def __init__(self, filepath):
        self.fw = FileWindow(filepath)

    def run(self):
        ch = -1

        while ch != 27:
            if ch == uc.KEY_UP:
                self.fw.delta_move(0, -1)
            elif ch == uc.KEY_DOWN:
                self.fw.delta_move(0, 1)
            elif ch == uc.KEY_LEFT:
                self.fw.delta_move(-1, 0)
            elif ch == uc.KEY_RIGHT:
                self.fw.delta_move(1, 0)
            elif ch == uc.KEY_BACKSPACE:
                self.fw.erase()
            elif ch == 10:
                self.fw.add_row()
            elif ch > 0:
                self.fw.write(chr(ch))
            self.fw.draw()
            ch = uc.getch()

        if self.fw.file is None:
            ans = self.fw.ask('Do you want to save the file?')
            if ans == 'n':
                self.fw.close()
                return
            p = self.fw.ask_path()
            if not pl.Path(p).exists():
                self.fw.file = open(p, 'w')
                self.fw.save()
                self.fw.close()
                return
            ans = self.fw.ask('There\'s an existing file. Overwrite it?')
            if ans == 'y':
                self.fw.file = open(p, 'w')
            else:
                is_free = False
                num = 0
                p_path = pl.Path(p).parent
                while not is_free:
                    num = randint(1, 10 ** 9)
                    is_free = not (p_path / str(num)).exists()
                self.fw.file = open(str(p_path / str(num)), 'w')
            self.fw.save()
        self.fw.close()
        if self.fw.file is not None:
            print('The file has been saved as ' + self.fw.file.name)


def print_manual():
    print('Use ESC to leave.',
          'To move across the file, use arrows.',
          'Everything else works as usual.',
          'The editor supports only English language, do not use alt-sequences. It will be treated as ESC',
          sep='\n')


if __name__ == "__main__":
    parse_machine = argparse.ArgumentParser(description='MyEditor is a program to edit and read files')
    parse_machine.add_argument('-m', '--manual', action='store_true',
                               help='the program will output a page about how to work in the editor')
    parse_machine.add_argument('-p', metavar='path_to_file', type=str, default=None, required=False,
                               help="Path to a file which you want to open in the editor")

    args = parse_machine.parse_args()

    if hasattr(args, 'help'):
        parse_machine.print_help()
    elif args.manual:
        print_manual()
    else:
        editor = MyEditor(args.p)
        editor.run()
