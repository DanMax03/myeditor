import unicurses as uc
import pathlib as pl
from random import randint
from geometry import *
import os
import time


def log(s):
    with open('log', 'r+') as f:
        f.writelines(s)
    time.sleep(1)


class FileWindow:
    _backup_ext = 'bkp'
    _backup_limit = 100  # Every _backup_limit changes we will save everything in bkp file

    def __init__(self, filepath=None):
        if filepath is None:
            self.file = None
            is_free = False
            num = -1
            while not is_free:
                num = randint(1, 10**9)
                is_free = not pl.Path('.' + str(num) + '.' + FileWindow._backup_ext).exists()
            self.backup_file = open('.' + str(num) + '.' + FileWindow._backup_ext, 'w')
        else:
            p = pl.Path(filepath)
            if not p.parent.exists():
                raise Exception("The path doesn't exists")
            if p.is_dir():
                raise Exception("The path points to a directory, not a file")
            if not p.exists():
                f = open(str(p), 'w')
                f.close()
            self.file = open(str(p), 'r+')
            self.backup_file = open(str(p.parent) + '/.' + p.name + '.' + FileWindow._backup_ext, 'w')
        self.text = list() if self.file is None else list(map(str.strip, self.file.readlines()))
        if len(self.text) == 0:
            self.text.append('')
        self.backup_counter = 0
        self.caret = Point(0, 0)
        self.anchor = 0
        stdscr = uc.initscr()
        self.scr = stdscr
        uc.start_color()
        uc.use_default_colors()
        uc.init_pair(1, uc.COLOR_WHITE, uc.COLOR_BLACK)
        uc.cbreak()
        uc.noecho()
        uc.nodelay(self.scr, True)
        uc.keypad(self.scr, True)

    def close(self):
        if self.backup_counter > 0:
            self.backup_save()
        if self.file is not None:
            self.save()
            self.file.close()
        self.backup_file.close()
        os.remove(self.backup_file.name)
        uc.endwin()

    def backup_save(self):
        self.backup_file.truncate(0)
        self.backup_file.write('\n'.join(self.text))
        self.backup_counter = 0

    def save(self):
        self.file.truncate(0)
        self.file.write('\n'.join(self.text))

    def write(self, s):
        h = self.caret.y
        self.text[h] = self.text[h][:self.caret.x] + s + self.text[h][self.caret.x:]
        self.backup_counter += 1
        self.caret.x += 1
        if self.backup_counter == self._backup_limit:
            self.backup_save()

    def erase(self):
        y = self.caret.y
        if self.caret.x == 0:
            if y == 0:
                if len(self.text) == 1:
                    return
                self.text.pop(0)
                return
            self.caret.y -= 1
            self.caret.x = len(self.text[y - 1])
            self.text[y - 1] += self.text[y]
            self.text.pop(y)
        else:
            self.text[y] = self.text[y][:self.caret.x - 1] + self.text[y][self.caret.x:]
            if self.caret.x > len(self.text[y]):
                self.caret.x -= 1

    def fit_position(self):
        if self.caret.y < 0:
            self.caret.y = 0
        elif self.caret.y >= len(self.text):
            self.caret.y = len(self.text) - 1
        if self.caret.x < 0:
            self.caret.x = 0
        elif self.caret.x > len(self.text[self.caret.y]):
            self.caret.x = len(self.text[self.caret.y])
        if self.anchor > self.caret.y:
            self.anchor = self.caret.y
        else:
            h = uc.getmaxyx(self.scr)[0]
            if self.caret.y - self.anchor >= h:
                self.anchor = max(0, self.caret - h)

    def add_row(self):
        y = self.caret.y
        self.text.insert(y + 1, '')
        self.text[y + 1] = self.text[y][self.caret.x:]
        self.text[y] = self.text[y][:self.caret.x]
        self.caret.x = 0
        self.caret.y += 1

    def delta_move(self, dx, dy):
        self.caret += Point(dx, dy)
        self.fit_position()

    def move(self, x, y):
        self.caret = Point(x, y)
        self.fit_position()

    def _draw(self):
        t = uc.getmaxyx(self.scr)
        far_point = Point(t[1], t[0])
        uc.erase()
        k = 0
        for i in range(self.anchor, min(len(self.text), self.anchor + far_point.y - 1)):
            uc.move(k, 0)
            uc.addch('*', uc.A_BOLD)
            uc.addstr(self.text[i])
            k += 1

    def draw(self):
        self._draw()
        uc.move(self.caret.y - self.anchor, self.caret.x + 1)
        uc.refresh()

    def ask(self, msg):
        msg += ' [y/n]: '
        ch = 'a'
        t = uc.getmaxyx(self.scr)
        far_point = Point(t[1], t[0])
        uc.move(far_point.y - 1, 0)
        uc.addstr(msg, uc.A_BOLD)
        uc.refresh()
        while ch != 'y' and ch != 'n':
            ch = uc.getch()
            if ch < 0:
                continue
            ch = chr(ch).lower()
        return ch

    def ask_path(self):
        correct = False
        ans = ''
        msg = 'Please enter a path to the file: '
        uc.nodelay(self.scr, False)
        while not correct:
            self._draw()
            t = uc.getmaxyx(self.scr)
            far_point = Point(t[1], t[0])
            uc.move(far_point.y - 1, 0)
            uc.addstr(msg)
            uc.echo()
            uc.refresh()
            ans = uc.getstr()
            uc.noecho()
            try:
                p = pl.Path(ans)
                correct = not p.is_dir()
            except Exception:
                pass
            if not correct:
                msg = 'The wrong path is given. Try again: '
        return ans
